const contentTabs = document.querySelectorAll('.content');
const contentLinks = document.querySelectorAll('.nav');
const audio = document.getElementById('ongaku');
const currentTime = document.getElementById('current-time');
const playIcon = document.getElementById('play-icon');
let state = 'play';
audio.volume = 0.05;

function formatTime(secs) {
    const minutes = Math.floor(secs / 60);
    const seconds = Math.floor(secs % 60);
    const returnedSeconds = seconds < 10 ? `0${seconds}` : `${seconds}`;
    return `${minutes}:${returnedSeconds}`;
}

function switchTab(e) {
    let tab = e.target.dataset.tab;
    // hide everything
    contentTabs.forEach((content) => {
        content.style.display = "none";
    });
    // make sure every link doesn't have active class
    contentLinks.forEach((link) => {
        link.classList.remove('active');
    });
    // show the tab with id that corresponds to data-tab
    // and change the 'current tab' name
    document.getElementById('current-tab').textContent = `✧ ${tab}`;
    document.getElementById(tab).style.display = 'block';
    document.querySelector(`[data-tab=${tab}]`).classList.add('active');
}

function displayDuration() {
    currentTime.textContent = formatTime(audio.currentTime);
}

contentLinks.forEach((link) => {
    link.addEventListener('click', switchTab);
});

audio.addEventListener('timeupdate', () => {
    displayDuration();
});

playIcon.addEventListener('click', () => {
    if (state === 'play') {
        audio.play();
        playIcon.textContent = '⏸︎';
        state = 'pause';
    } else {
        audio.pause();
        playIcon.textContent = '▶';
        state = 'play';
    }
})

window.onload = () => {
    document.querySelector('[data-tab="about"]').click();
}