# Neocities
source code for my neocities webpage. it's a big work in progress. i'm sorry.

`jelly`: my kpop account's little landing page. view [here](https://aleph.coffee/jelly/).

`startpage`: my startpage. repo [here](https://gitlab.com/rissu/startpage). view [here](https://aleph.coffee/startpage)

licensed under the MIT.